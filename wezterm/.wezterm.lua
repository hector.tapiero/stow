-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
  config = wezterm.config_builder()
end

-- This is where you actually apply your config choices

-- For example, changing the color scheme:
config.color_scheme = 'AdventureTime'
-- https://wezfurlong.org/wezterm/config/lua/config/background.html#source-definition
config.background = {
  {
    source = { File = { path = '/Users/hectortapiero/Projects/stow/wezterm/galaxy.jpeg' } },
    hsb = {
      brightness = 0.05,
      saturation = 1.00,
    },
    opacity = 0.9,
    horizontal_align = 'Center',
    height = 'Cover',
    width = 'Cover',
  },
}
-- and finally, return the configuration to wezterm
return config